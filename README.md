# Shantybot

## What does it do?

This is a bot that takes lyrics from `corpus.txt`, and generates sentences based on a Markov chain trained on that text.

This repeats until it has generated two stanzas, each with four lines with an ABAB/CDCD rhyming structure and roughly similar numbers of syllables.

It then posts the completed two stanza sea shanty to Mastodon under a content warning.

To make this work on a regular basis, just set a cron job to tell it to post when you want it to.

## Also

Update: Just in case there's a problem, I added a script that allows me to delete bad sea shanties, because *YOU NEVER KNOW* when a bot will say something bad.

## Live version

See https://botsin.space/@shantybot for a live version

I took the sea shanties from http://www.jsward.com/shanty/ to build the corpus
