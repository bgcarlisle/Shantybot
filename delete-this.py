from mastodon import Mastodon

mastodon = Mastodon(
    access_token = 'shantybot_usercred.secret',
    api_base_url = 'https://botsin.space'
)

notifications = mastodon.notifications()

for notif in notifications:
  if notif.type == "mention":
    print(notif["id"])
    print(notif.status.content)
    print(notif.account.acct)
    if "!deletethis" in notif.status.content and notif.account.acct == "bgcarlisle@scholar.social":
      id_to_delete = notif.get("status").get("in_reply_to_id")
      print(notif["id"])
      mastodon.notifications_dismiss(notif["id"])
      mastodon.status_delete(id_to_delete)
    if "!replacethis" in notif.status.content and notif.account.acct == "bgcarlisle@scholar.social":
      id_to_delete = notif.get("status").get("in_reply_to_id")
      print(notif["id"])
      mastodon.notifications_dismiss(notif["id"])
      mastodon.status_delete(id_to_delete)
      os.system('python3 shantybot.py')

